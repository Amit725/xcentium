# xcentium

> Code Challenge for XCentium

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
#notes

1. The color contrast will be a accessibility issue. Since, requirement was using the same, I didn't change it. .
2. I used webpack template to generate the solution. It had everything I needed. I can create from scratch if needed.
3. For the shaded areas, I am just placing empty div.


